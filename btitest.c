// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (C) 2019  Arm Limited
 * Original author: Dave Martin <Dave.Martin@arm.com>
 */

#include "system.h"

#include <linux/errno.h>
#include <linux/auxvec.h>
#include <linux/signal.h>
#include <asm/hwcap.h>
#include <asm/sigcontext.h>
#include <asm/ucontext.h>

typedef struct ucontext ucontext_t;

#include "btitest.h"
#include "compiler.h"
#include "signal.h"

static void fdputs(int fd, const char *str)
{
	size_t len = 0;
	const char *p = str;

	while (*p++)
		++len;

	write(fd, str, len);
}

static void putstr(const char *str)
{
	fdputs(1, str);
}

#define puttestname(test_name, trampoline_name) do {	\
	putstr(test_name);				\
	putstr("/");					\
	putstr(trampoline_name);			\
} while (0)

static const char *volatile current_test_name;
static const char *volatile current_trampoline_name;
static volatile int sigill_expected, sigill_received;

static void handler(int n, siginfo_t *si __always_unused,
		    void *uc_ __always_unused)
{
	ucontext_t *uc = uc_;

	putstr("\t[SIGILL in ");
	puttestname(current_test_name, current_trampoline_name);
	putstr(", BTYPE=");
	write(1, &"00011011"[((uc->uc_mcontext.pstate & PSR_BTYPE_MASK)
			      >> PSR_BTYPE_SHIFT) * 2], 2);
	if (!sigill_expected) {
		putstr("]\n");
		puttestname(current_test_name, current_trampoline_name);
		putstr(":\tFAIL (unexpected SIGILL)\n");
		exit(128 + n);
	}

	putstr(" (expected)]\n");
	sigill_received = 1;
	/* zap BTYPE so that resuming the faulting code will work */
	uc->uc_mcontext.pstate &= ~PSR_BTYPE_MASK;
}

static int skip_all = 0;

static void __do_test(void (*trampoline)(void (*)(void)),
		      void (*fn)(void),
		      const char *trampoline_name,
		      const char *name,
		      int expect_sigill)
{
	if (skip_all) {
		puttestname(name, trampoline_name);
		putstr(":\tSKIPPED\n");

		return;
	}

	/* Branch Target exceptions should only happen in BTI binaries: */
	if (!BTI)
		expect_sigill = 0;

	sigill_expected = expect_sigill;
	sigill_received = 0;
	current_test_name = name;
	current_trampoline_name = trampoline_name;

	trampoline(fn);

	puttestname(name, trampoline_name);
	if (expect_sigill && !sigill_received) {
		putstr(":\tFAIL (no SIGILL)\n");
		exit(1);
	} else {
		putstr(":\tPASS\n");
	}
}

#define do_test(expect_sigill_br_x0,					\
		expect_sigill_br_x16,					\
		expect_sigill_blr,					\
		name)							\
do {									\
	__do_test(call_using_br_x0, name, "call_using_br_x0", #name,	\
		  expect_sigill_br_x0);					\
	__do_test(call_using_br_x16, name, "call_using_br_x16", #name,	\
		  expect_sigill_br_x16);				\
	__do_test(call_using_blr, name, "call_using_blr", #name,	\
		  expect_sigill_blr);					\
} while (0)

void start(int *argcp)
{
	struct sigaction sa;
	void *const *p;
	const struct auxv_entry {
		unsigned long type;
		unsigned long val;
	} *auxv;
	unsigned long hwcap = 0, hwcap2 = 0;

	/* Gross hack for finding AT_HWCAP2 from the initial process stack: */
	p = (void *const *)argcp + 1 + *argcp + 1; /* start of environment */
	while (*p++) ; /* step over environment */
	for (auxv = (const struct auxv_entry *)p; auxv->type != AT_NULL;
			++auxv)
		switch (auxv->type) {
		case AT_HWCAP:	hwcap = auxv->val; break;
		case AT_HWCAP2:	hwcap2 = auxv->val; break;
		}

	if (hwcap & HWCAP_PACA)
		putstr("HWCAP_PACA present\n");
	else
		putstr("HWCAP_PACA not present\n");

	if (hwcap2 & HWCAP2_BTI) {
		putstr("HWCAP2_BTI present\n");
		if (!(hwcap & HWCAP_PACA))
			putstr("Bad hardware?  Expect problems.\n");
	} else {
		putstr("HWCAP2_BTI not present\n");
		skip_all = 1;
	}

	putstr("Test binary");
	if (!BTI)
		putstr(" not");
	putstr(" built for BTI\n");

	putstr("---\n");

	sa.sa_handler = (sighandler_t)(void *)handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGILL, &sa, NULL);
	sigaddset(&sa.sa_mask, SIGILL);
	sigprocmask(SIG_UNBLOCK, &sa.sa_mask, NULL);

	do_test(1, 1, 1, nohint_func);
	do_test(1, 1, 1, bti_none_func);
	do_test(1, 0, 0, bti_c_func);
	do_test(0, 0, 1, bti_j_func);
	do_test(0, 0, 0, bti_jc_func);
	do_test(1, 0, 0, paciasp_func);

	putstr("\nNo test failures.\n");
	exit(0);
}
