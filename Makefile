# SPDX-License-Identifier: GPL-2.0-only
# Copyright (C) 2019  Arm Limited
# Original author: Dave Martin <Dave.Martin@arm.com>

CFLAGS = -O2 -g
CPPFLAGS =
LDFLAGS =
CROSS_COMPILE = aarch64-linux-gnu-
CC = $(CROSS_COMPILE)gcc

TESTS = btitest nobtitest

CFLAGS_NOBTI = -DBTI=0
CFLAGS_BTI = -mbranch-protection=standard -DBTI=1

CFLAGS_COMMON = -fno-pie -no-pie -ffreestanding -Wall -Wextra -MMD $(CFLAGS)
LDFLAGS_COMMON = -static -nostdlib $(LDFLAGS)

BTI_CC_COMMAND = \
	$(CC) $(CPPFLAGS) $(CFLAGS_BTI) $(CFLAGS_COMMON) -c -o $@ $<
NOBTI_CC_COMMAND = \
	$(CC) $(CPPFLAGS) $(CFLAGS_NOBTI) $(CFLAGS_COMMON) -c -o $@ $<

all: FORCE $(TESTS) ;

FORCE: ;

-include *.d

%-bti.o: %.c
	$(BTI_CC_COMMAND)

%-bti.o: %.S
	$(BTI_CC_COMMAND)

%-nobti.o: %.c
	$(NOBTI_CC_COMMAND)

%-nobti.o: %.S
	$(NOBTI_CC_COMMAND)

BTI_OBJS =					\
	btitest-bti.o				\
	signal-bti.o				\
	start-bti.o				\
	syscall-bti.o				\
	system-bti.o				\
	teststubs-bti.o				\
	trampoline-bti.o
btitest: $(BTI_OBJS)
CFLAGS-btitest = $(CFLAGS_BTI)

NOBTI_OBJS =					\
	btitest-nobti.o				\
	signal-nobti.o				\
	start-nobti.o				\
	syscall-nobti.o				\
	system-nobti.o				\
	teststubs-nobti.o			\
	trampoline-nobti.o
nobtitest: $(NOBTI_OBJS)
CFLAGS-nobtitest = $(CFLAGS_NOBTI)

$(TESTS):
	$(CC) $(CPPFLAGS) $(CFLAGS-$@) $(CFLAGS_COMMON) $(LDFLAGS_COMMON) -o $@ $^

clean: FORCE
	$(RM) $(TESTS) *.[od]
